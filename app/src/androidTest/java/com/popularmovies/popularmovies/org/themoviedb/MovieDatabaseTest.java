package com.popularmovies.popularmovies.org.themoviedb;

import org.junit.Test;

import java.net.URL;

import static org.junit.Assert.*;

public class MovieDatabaseTest {

    @Test
    public void getPopularMoviesURL_ShouldIncludeAPIKey() throws Exception {
        String apiKey = "12345";

        URL popularMoviesURL = new URL("http://api.themoviedb.org/3/movie/popular?api_key=" + apiKey);
        assertEquals(popularMoviesURL, MovieDatabase.getPopularMoviesURL(apiKey));
    }

    @Test
    public void getTopRatedMoviesURL_ShouldIncludeAPIKey() throws Exception {
        String apiKey = "12345";

        URL popularMoviesURL = new URL("http://api.themoviedb.org/3/movie/top_rated?api_key=" + apiKey);
        assertEquals(popularMoviesURL, MovieDatabase.getTopRatedMoviesURL(apiKey));
    }

}