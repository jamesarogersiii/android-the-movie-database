package com.popularmovies.popularmovies.org.themoviedb;

import android.content.SharedPreferences;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.popularmovies.popularmovies.org.themoviedb.MovieDatabasePreferences.API_KEY;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MovieDatabasePreferencesTest {

    @Mock
    SharedPreferences sharedPreferences;


    @Test
    public void isAPIKeyAvailable_KeyDoesNotExist_ShouldReturnFalse() throws Exception {
        when(sharedPreferences.contains(API_KEY)).thenReturn(false);
        MovieDatabasePreferences movieDatabasePreferences = new MovieDatabasePreferences(sharedPreferences);
        assertFalse(movieDatabasePreferences.isAPIKeyAvailable());
    }

    @Test
    public void isAPIKeyAvailable_KeyExists_ShouldReturnTrue() throws Exception {
        when(sharedPreferences.contains(API_KEY)).thenReturn(true);
        MovieDatabasePreferences movieDatabasePreferences = new MovieDatabasePreferences(sharedPreferences);
        assertTrue(movieDatabasePreferences.isAPIKeyAvailable());
    }

    @Mock
    SharedPreferences.Editor sharedPreferencesEditor;

    @Test
    public void setAPIKey_SavesAPIKeyInPrefernces() throws Exception {
        String apiKey = "12345";

        when(sharedPreferences.edit()).thenReturn(sharedPreferencesEditor);
        MovieDatabasePreferences movieDatabasePreferences = new MovieDatabasePreferences(sharedPreferences);
        movieDatabasePreferences.setAPIKey(apiKey);
        verify(sharedPreferencesEditor, times(1)).putString(API_KEY, apiKey);
        verify(sharedPreferencesEditor, times(1)).apply();

    }

    @Test
    public void getAPIKey_RetrievesAPIKeyFromPreferences() throws Exception {
        String apiKey = "12345";

        when(sharedPreferences.getString(API_KEY, null)).thenReturn(apiKey);
        MovieDatabasePreferences movieDatabasePreferences = new MovieDatabasePreferences(sharedPreferences);
        assertEquals(movieDatabasePreferences.getAPIKey(), apiKey);
    }

}