package com.popularmovies.popularmovies.org.themoviedb;

import com.popularmovies.popularmovies.utilities.NetworkUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mock;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieDownloadTaskTest {

    @Mock
    MovieDownloadTask.MovieListener movieListener;
    @Mock
    NetworkUtils networkUtils;

    private final String movieJSON = "{" +
            "  \\\"page\\\": 1," +
            "  \\\"total_results\\\": 19676," +
            "  \\\"total_pages\\\": 984," +
            "  \\\"results\\\": [" +
            "    {" +
            "      \\\"vote_count\\\": 1724," +
            "      \\\"id\\\": 297762," +
            "      \\\"video\\\": false," +
            "      \\\"vote_average\\\": 7," +
            "      \\\"title\\\": \\\"Wonder Woman\\\"," +
            "      \\\"popularity\\\": 99.147603," +
            "      \\\"poster_path\\\": \\\"/gfJGlDaHuWimErCr5Ql0I8x9QSy.jpg\\\"," +
            "      \\\"original_language\\\": \\\"en\\\"," +
            "      \\\"original_title\\\": \\\"Wonder Woman\\\"," +
            "      \\\"genre_ids\\\": [28,12,14,878]," +
            "      \\\"backdrop_path\\\": \\\"/hA5oCgvgCxj5MEWcLpjXXTwEANF.jpg\\\"," +
            "      \\\"adult\\\": false," +
            "      \\\"overview\\\": \\\"An Amazon princess comes to the world of Man to become the greatest of the female superheroes.\\\"," +
            "      \\\"release_date\\\": \\\"2017-05-30\\\"" +
            "    }" +
            "  ]" +
            "}";

    @Test
    public void downloadMoviesFromService_ProvidesListenerMovies(){
        try {

            URL serviceUrl = new URL("http://api.themoviedb.org/3/movie/popular");
            MovieDownloadTask movieDownloadTask = new MovieDownloadTask(movieListener, serviceUrl, networkUtils);
            JSONObject movieJSONObject = new JSONObject(movieJSON);
            Movie movie = MovieDatabase.movieFromJSON(movieJSONObject, new SimpleDateFormat("yyyy-MM-dd", Locale.US));
            ArrayList<Movie> movies = new ArrayList<>();
            movies.add(movie);

            when(networkUtils.getResponseFromHttpUrl(serviceUrl)).thenReturn(movieJSON);

            movieDownloadTask.execute();

            verify(movieListener, times(1)).onMoviesAvailable(movies, MovieDownloadTask.MovieListener.RESULT_OK);

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }


    }



}