package com.popularmovies.popularmovies;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import com.popularmovies.popularmovies.org.themoviedb.Movie;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class MovieDetailActivity extends AppCompatActivity {

    private TextView titleTextView;
    private TextView releaseDateTextView;
    private TextView userRatingTextView;
    private TextView descriptionTextView;
    private ImageView posterImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_detail_activity);

        titleTextView = (TextView) findViewById(R.id.titleTextView);
        userRatingTextView = (TextView) findViewById(R.id.userRatingTextView);
        releaseDateTextView = (TextView) findViewById(R.id.releaseDateTextView);
        descriptionTextView = (TextView) findViewById(R.id.descriptionTextView);
        posterImageView = (ImageView) findViewById(R.id.posterImageView);

        Movie movie = (Movie) getIntent().getSerializableExtra(Movie.MOVIE_KEY);
        setMovie(movie);
    }

    private void setMovie(Movie movie){

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy", Locale.US);
        String releaseDate = dateFormat.format(movie.releaseDate);
        releaseDateTextView.setText(releaseDate);

        userRatingTextView.setText(getString(R.string.movie_rating, movie.voteAverage));
        titleTextView.setText(movie.originalTitle);
        Picasso.with(getApplicationContext()).load(movie.posterPath).into(posterImageView);
        descriptionTextView.setText(movie.overview);

    }

}
