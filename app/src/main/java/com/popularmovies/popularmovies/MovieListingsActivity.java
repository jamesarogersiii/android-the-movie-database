package com.popularmovies.popularmovies;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.popularmovies.popularmovies.org.themoviedb.Movie;
import com.popularmovies.popularmovies.org.themoviedb.MovieDatabase;
import com.popularmovies.popularmovies.org.themoviedb.MovieDatabasePreferences;
import com.popularmovies.popularmovies.org.themoviedb.MovieDownloadTask;
import com.popularmovies.popularmovies.org.themoviedb.MovieFactory;
import com.popularmovies.popularmovies.utilities.StringUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MovieListingsActivity extends AppCompatActivity implements MovieDownloadTask.MovieListener, MovieDatabaseKeyDownloadTask.KeyListener, View.OnClickListener, MovieClickListener {

    private final static String APPLICATION_PREFERENCES = "Application Preferences";

    private RecyclerView movieListingRecyclerView;
    private MovieRecyclerViewAdapter movieRecyclerViewAdapter;
    private ProgressBar progressBar;
    private ViewGroup movieDatabaseAPIKeyLayout;
    private TextView apiKeyEditText;
    private View errorMessageLayout;
    private MovieDatabasePreferences movieDatabasePreferences;

    private static final String TAG = MovieListingsActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_listings_activity);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        errorMessageLayout = findViewById(R.id.error_message);
        Button retryButton = (Button) findViewById(R.id.retryButton);
        retryButton.setOnClickListener(this);
        movieDatabasePreferences = MovieFactory.makeMovieDatabasePreferences(getPreferences());

        setupAPIKeyView();
        setupRecyclerView();
        initialize(savedInstanceState, movieDatabasePreferences);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.movie_listing_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_item_popular:
                loadPopularMovies();
                break;

            case R.id.menu_item_top_rated:
                loadTopRatedMovies();
                break;

        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {

        if(movieRecyclerViewAdapter.getMovies() != null) {
            outState.putSerializable(Movie.MOVIE_KEY, movieRecyclerViewAdapter.getMovies());
        }

        super.onSaveInstanceState(outState);
    }

    private void initialize(Bundle savedInstanceState, MovieDatabasePreferences movieDatabasePreferences){

        ArrayList<Movie> movies = null;
        if(savedInstanceState != null){
            //noinspection unchecked
            movies = (ArrayList<Movie>) savedInstanceState.getSerializable(Movie.MOVIE_KEY);
        }

        if(movieDatabasePreferences.isAPIKeyAvailable()) {

            if(movies != null){
                showMovies(movies);
            }
            else {
                loadPopularMovies();
            }
        }
        else{
            downloadMovieDatabaseAPIKey();
        }
    }

    private void setupRecyclerView(){
        movieListingRecyclerView = (RecyclerView) findViewById(R.id.movieListingRecyclerView);
        movieListingRecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), numberOfColumns()));
        movieRecyclerViewAdapter = new MovieRecyclerViewAdapter(Picasso.with(getApplicationContext()));
        movieListingRecyclerView.setAdapter(movieRecyclerViewAdapter);
        movieListingRecyclerView.setHasFixedSize(true);
        movieRecyclerViewAdapter.setMovieClickListener(this);
    }

    private int numberOfColumns() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int widthDivider = 600;
        int width = displayMetrics.widthPixels;
        int nColumns = width / widthDivider;
        if (nColumns < 2) return 2;
        return nColumns;
    }

    private void setupAPIKeyView(){
        movieDatabaseAPIKeyLayout = (ViewGroup) findViewById(R.id.movieDatabaseAPIKeyLayout);
        apiKeyEditText = (EditText) findViewById(R.id.apiKeyEditText);
        Button saveAPIKeyButton = (Button) findViewById(R.id.saveButton);
        saveAPIKeyButton.setOnClickListener(this);
        Button signupButton = (Button) findViewById(R.id.signUpButton);
        signupButton.setOnClickListener(this);
    }

    private SharedPreferences getPreferences(){
       return getSharedPreferences(APPLICATION_PREFERENCES, MODE_PRIVATE);
    }

    private void loadPopularMovies(){
        Log.d(TAG, "loadPopularMovies");
        String apiKey = movieDatabasePreferences.getAPIKey();
        MovieDownloadTask movieDownloadTask = MovieFactory.makePopularMovieDownloadTask(this, apiKey);
        movieDownloadTask.execute();
        showProgressBar();
    }

    private void loadTopRatedMovies(){
        Log.d(TAG, "loadPopularMovies");
        String apiKey = movieDatabasePreferences.getAPIKey();
        MovieDownloadTask movieDownloadTask = MovieFactory.makeTopRatedMovieDownloadTask(this, apiKey);
        movieDownloadTask.execute();
        showProgressBar();
    }

    private void saveAPIKey(String apiKey){
        MovieDatabasePreferences movieDatabasePreferences = new MovieDatabasePreferences(getPreferences());
        movieDatabasePreferences.setAPIKey(apiKey);
    }

    private void hideViews(){
        progressBar.setVisibility(View.GONE);
        errorMessageLayout.setVisibility(View.GONE);
        movieListingRecyclerView.setVisibility(View.GONE);
        movieDatabaseAPIKeyLayout.setVisibility(View.GONE);
    }

    private void showMovies(ArrayList<Movie> movies){
        Log.d(TAG, "showMovies");

        hideViews();
        movieListingRecyclerView.setVisibility(View.VISIBLE);
        movieRecyclerViewAdapter.setMovies(movies);
    }

    private void showError(){
        Log.d(TAG, "showError");

        hideViews();
        errorMessageLayout.setVisibility(View.VISIBLE);

    }

    private void showProgressBar(){
        Log.d(TAG, "showProgressBar");

        hideViews();
        progressBar.setVisibility(View.VISIBLE);
    }


    private void signUp(){
        Intent signupIntent = new Intent(Intent.ACTION_VIEW);
        signupIntent.setData(MovieDatabase.MOVIE_SIGNUP_URI);
        if(signupIntent.resolveActivity(getPackageManager()) != null){
            startActivity(signupIntent);
        }
    }

    @Override
    public void onMoviesAvailable(ArrayList<Movie> movies, int result) {
        Log.d(TAG, "onMoviesAvailable " + result);

        if(result == MovieDownloadTask.MovieListener.RESULT_OK) {
            showMovies(movies);
            movieRecyclerViewAdapter.setMovies(movies);
        }
        else{
            showError();
        }
    }

    @Override
    public void onMovieClick(Movie movie) {
        Log.d(TAG, "onMovieClick " + movie.title);

        Intent movieDetailIntent = new Intent(this, MovieDetailActivity.class);
        movieDetailIntent.putExtra(Movie.MOVIE_KEY, movie);
        startActivity(movieDetailIntent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.saveButton:
                if(apiKeyEditText.getText().length() > 0){
                    saveAPIKey(apiKeyEditText.getText().toString());
                    loadPopularMovies();
                }

                break;

            case R.id.signUpButton:
                signUp();
                break;

            case R.id.retryButton:
                downloadMovieDatabaseAPIKey();
                break;

        }
    }

    private void downloadMovieDatabaseAPIKey(){
        movieDatabasePreferences.setAPIKey(null);
        MovieFactory.makeMovieDatabaseKeyDownloadTask(this).execute();
    }

    @Override
    public void onKeyAvailable(String apiKey) {
        if(StringUtil.isEmpty(apiKey)){
            showError();
        }
        else {
            movieDatabasePreferences.setAPIKey(apiKey.trim());
            loadPopularMovies();
        }
    }
}
