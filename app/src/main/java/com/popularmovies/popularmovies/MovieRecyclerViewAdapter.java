package com.popularmovies.popularmovies;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.popularmovies.popularmovies.org.themoviedb.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

class MovieRecyclerViewAdapter extends RecyclerView.Adapter<MovieRecyclerViewAdapter.MovieViewHolder> {

    private ArrayList<Movie> movies;
    private MovieClickListener movieClickListener;

    private final Picasso picasso;

    public MovieRecyclerViewAdapter(Picasso picasso){
        this.picasso = picasso;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_view, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        Movie movie = movies.get(position);
        holder.bindView(movie);
    }

    @Override
    public int getItemCount() {

        if(movies == null) return 0;

        return movies.size();
    }

    public void setMovies(ArrayList<Movie> movies){
        this.movies = movies;
        notifyDataSetChanged();
    }

    public void setMovieClickListener(MovieClickListener movieClickListener){
        this.movieClickListener = movieClickListener;
    }

    public ArrayList<Movie> getMovies(){
        return movies;
    }

    class MovieViewHolder extends RecyclerView.ViewHolder{

        private final ImageView posterImageView;

        public MovieViewHolder(View itemView) {
            super(itemView);

            posterImageView = (ImageView) itemView.findViewById(R.id.posterImageView);
        }

        public void bindView(final Movie movie){

            picasso.load(movie.posterPath).into(posterImageView);

            posterImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(movieClickListener != null){
                        movieClickListener.onMovieClick(movie);
                    }
                }
            });

        }
    }

}

interface MovieClickListener{
    void onMovieClick(Movie movie);
}
