package com.popularmovies.popularmovies.org.themoviedb;

import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;


public class MovieDatabase {

    private final static Uri MOVIE_DATABASE_URI = Uri.parse("http://api.themoviedb.org");
    private final static String POPULAR_MOVIE_PATH = "3/movie/popular";
    private final static String TOP_RATED_MOVIE_PATH = "3/movie/top_rated";
    private final static String API_KEY_PARAMETER = "api_key";
    public final static Uri MOVIE_SIGNUP_URI = Uri.parse("https://www.themoviedb.org/account/signup");
    private final static String MOVIE_IMAGE_BASE_PATH = "http://image.tmdb.org/t/p/w342";

    private final static String TAG = MovieDatabase.class.getCanonicalName();

    @Nullable
    static URL getPopularMoviesURL(String apiKey){

       Uri uri = MOVIE_DATABASE_URI.buildUpon()
                .path(POPULAR_MOVIE_PATH)
                .appendQueryParameter(API_KEY_PARAMETER, apiKey).build();

        try {
            return new URL(uri.toString());
        } catch (MalformedURLException e) {
            Log.d(TAG, e.getMessage());
        }

        return null;

    }

    @Nullable
    static URL getTopRatedMoviesURL(String apiKey){

        Uri uri = MOVIE_DATABASE_URI.buildUpon()
                .path(TOP_RATED_MOVIE_PATH)
                .appendQueryParameter(API_KEY_PARAMETER, apiKey).build();

        try {
            return new URL(uri.toString());
        } catch (MalformedURLException e) {
            Log.d(TAG, e.getMessage());
        }

        return null;
    }

    public static Movie movieFromJSON(JSONObject movieJSON, SimpleDateFormat dateFormat){

        try {
            Movie movie = new Movie();

            movie.vote = movieJSON.getInt("vote_count");
            movie.id = movieJSON.getInt("id");
            movie.voteAverage = movieJSON.getDouble("vote_average");
            movie.title = movieJSON.getString("title");
            movie.originalTitle = movieJSON.getString("original_title");
            movie.posterPath = MOVIE_IMAGE_BASE_PATH + movieJSON.getString("poster_path");
            movie.backdropPath = MOVIE_IMAGE_BASE_PATH + movieJSON.getString("backdrop_path");
            movie.overview = movieJSON.getString("overview");
            movie.releaseDate = dateFormat.parse(movieJSON.getString("release_date"));

            return movie;
        }
        catch (JSONException | ParseException e){
            Log.d(TAG, e.getMessage());
        }

        return null;
    }

}
