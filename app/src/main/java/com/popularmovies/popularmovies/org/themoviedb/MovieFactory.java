package com.popularmovies.popularmovies.org.themoviedb;

import android.content.SharedPreferences;
import com.popularmovies.popularmovies.MovieDatabaseKeyDownloadTask;
import com.popularmovies.popularmovies.utilities.NetworkUtils;

import java.net.URL;

public class MovieFactory {

    public static MovieDatabasePreferences makeMovieDatabasePreferences(SharedPreferences sharedPreferences){
        return new MovieDatabasePreferences(sharedPreferences);
    }

    public static MovieDatabaseKeyDownloadTask makeMovieDatabaseKeyDownloadTask(MovieDatabaseKeyDownloadTask.KeyListener listener){
        return new MovieDatabaseKeyDownloadTask(listener, new NetworkUtils());
    }

    public static MovieDownloadTask makePopularMovieDownloadTask(MovieDownloadTask.MovieListener movieListener, String apiKey){
        URL moviesUrl = MovieDatabase.getPopularMoviesURL(apiKey);
        return new MovieDownloadTask(movieListener, moviesUrl, new NetworkUtils());
    }

    public static MovieDownloadTask makeTopRatedMovieDownloadTask(MovieDownloadTask.MovieListener movieListener, String apiKey){
        URL moviesUrl = MovieDatabase.getTopRatedMoviesURL(apiKey);
        return new MovieDownloadTask(movieListener, moviesUrl, new NetworkUtils());
    }

}
