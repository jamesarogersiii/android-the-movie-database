package com.popularmovies.popularmovies.org.themoviedb;

import java.io.Serializable;
import java.util.Date;


@SuppressWarnings("unused")
public class Movie implements Serializable{

    public final static String MOVIE_KEY = "Movie Key";

    public int vote;
    public long id;
    public double voteAverage;
    public String title;
    public String originalTitle;
    public double popularity;
    public String posterPath;
    public String backdropPath;
    public String overview;
    public Date releaseDate;

}
