package com.popularmovies.popularmovies.org.themoviedb;

import android.content.SharedPreferences;

public class MovieDatabasePreferences {

    final static String API_KEY = "Movie Database API Key";
    private final SharedPreferences sharedPreferences;

    public MovieDatabasePreferences(SharedPreferences sharedPreferences){
        this.sharedPreferences = sharedPreferences;
    }

    public boolean isAPIKeyAvailable(){
        return sharedPreferences.contains(API_KEY);
    }

    public void setAPIKey(String value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(API_KEY, value);
        editor.apply();
    }

    public String getAPIKey(){
        return sharedPreferences.getString(API_KEY, null);
    }
}
