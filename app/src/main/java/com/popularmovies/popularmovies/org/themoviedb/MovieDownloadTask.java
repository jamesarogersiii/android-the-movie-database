package com.popularmovies.popularmovies.org.themoviedb;

import android.os.AsyncTask;
import android.util.Log;

import com.popularmovies.popularmovies.utilities.NetworkUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class MovieDownloadTask extends AsyncTask<Void, Void, ArrayList<Movie>> {

    private final MovieListener movieListener;
    private final static String TAG = MovieDownloadTask.class.getCanonicalName();

    private final URL url;
    private final NetworkUtils networkUtils;

    public MovieDownloadTask(MovieListener movieListener, URL url, NetworkUtils networkUtils){
        this.movieListener = movieListener;
        this.url = url;
        this.networkUtils = networkUtils;
    }

    @Override
    protected ArrayList<Movie> doInBackground(Void... params) {

        ArrayList<Movie> movieList = new ArrayList<>();

        try {
            Log.d(TAG, url.toString());
            String response = networkUtils.getResponseFromHttpUrl(url);
            Log.d(TAG, response);

            JSONObject responseJSON = new JSONObject(response);
            JSONArray moviesJSON = responseJSON.getJSONArray("results");

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            for(int i=0; i< moviesJSON.length(); ++i){
                JSONObject movieJSON = moviesJSON.getJSONObject(i);
                Movie movie = MovieDatabase.movieFromJSON(movieJSON, dateFormat);

                if(movie != null) {
                    movieList.add(movie);
                }
            }

            return movieList;

        } catch (java.io.IOException | JSONException e) {
            Log.d(TAG, e.getMessage());
        }

        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Movie> movies) {
        super.onPostExecute(movies);

        if(movieListener != null){
            int result = (movies == null)? MovieListener.RESULT_ERROR: MovieListener.RESULT_OK;
            movieListener.onMoviesAvailable(movies, result);
        }

    }

    public interface MovieListener {

        int RESULT_OK = 1;
        int RESULT_ERROR = 0;

        void onMoviesAvailable(ArrayList<Movie> movies, int result);
    }
}