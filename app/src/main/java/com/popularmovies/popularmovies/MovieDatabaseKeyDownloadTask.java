package com.popularmovies.popularmovies;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.popularmovies.popularmovies.utilities.NetworkUtils;

import java.io.IOException;
import java.net.URL;


public class MovieDatabaseKeyDownloadTask extends AsyncTask<Void, Void, String> {

    private final Uri API_KEY_URL = Uri.parse("https://www.fumdum.com/MovieDatabaseAPIKey");
    private final static String TAG = MovieDatabaseKeyDownloadTask.class.getCanonicalName();
    private final KeyListener movieKeyListener;
    private final NetworkUtils networkUtils;

    public MovieDatabaseKeyDownloadTask(KeyListener listener, NetworkUtils networkUtils){
        this.movieKeyListener = listener;
        this.networkUtils = networkUtils;

    }

    @Override
    protected String doInBackground(Void... params) {

        try {

            URL apiKeyUrl = new URL(API_KEY_URL.toString());
            String apiKey = networkUtils.getResponseFromHttpUrl(apiKeyUrl);
            Log.d(TAG, "API Key " + apiKey);
            return apiKey;

        } catch (IOException e) {
            Log.d(TAG, "Movie Database API key download failed");
        }

        return null;
    }

    @Override
    protected void onPostExecute(String apiKey) {
        super.onPostExecute(apiKey);

        if(movieKeyListener != null){
            movieKeyListener.onKeyAvailable(apiKey);
        }
    }

    public interface KeyListener {
        void onKeyAvailable(String apiKey);
    }
}
